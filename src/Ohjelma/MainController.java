package Ohjelma;

import Ohjelma.paaIkkuna.paaIkkuna;
import Ohjelma.paaIkkuna.paaIkkunaDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.SQLException;

public class MainController {

    // pääikkunanapit

    @FXML
    private Button btnMain1;
    @FXML
    private Button btnMain2;
    @FXML
    private Button btnMainExit;
    @FXML
    private TextField txtVarausID;


//*************************************************

    //********************************************************************
    //Pääikkunan taulukon alustus

    @FXML
    private void initialize() {

        varausIDColumn.setCellValueFactory(cellData -> cellData.getValue().varausIDProperty().asObject());
        asiakasIDColumn.setCellValueFactory(cellData -> cellData.getValue().asiakasIDProperty().asObject());
        toimipisteIDColumn.setCellValueFactory(cellData -> cellData.getValue().toimipisteIDProperty().asObject());
        etunimiColumn.setCellValueFactory(cellData -> cellData.getValue().etunimiProperty());
        sukuNimiColumn.setCellValueFactory(cellData -> cellData.getValue().sukunimiProperty());
        toimipisteColumn.setCellValueFactory(cellData -> cellData.getValue().toimipisteProperty());
        var_alkuColumn.setCellValueFactory(cellData -> cellData.getValue().var_alkuProperty());
        var_loppuColumn.setCellValueFactory(cellData -> cellData.getValue().var_loppuProperty());
        palveluiden_lkmColumn.setCellValueFactory(cellData -> cellData.getValue().palvelu_lkmProperty().asObject());
    }

    //pääikkunan taulukko
    @FXML
    private TableView<paaIkkuna> paaIkkunaTaulu;
    @FXML
    private TableColumn<paaIkkuna, Integer> varausIDColumn;
    @FXML
    private TableColumn<paaIkkuna, Integer> asiakasIDColumn;
    @FXML
    private TableColumn<paaIkkuna, Integer> toimipisteIDColumn;
    @FXML
    private TableColumn<paaIkkuna, String> etunimiColumn;
    @FXML
    private TableColumn<paaIkkuna, String> sukuNimiColumn;
    @FXML
    private TableColumn<paaIkkuna, String> toimipisteColumn;
    @FXML
    private TableColumn<paaIkkuna, String> var_alkuColumn;
    @FXML
    private TableColumn<paaIkkuna, String> var_loppuColumn;
    @FXML
    private TableColumn<paaIkkuna, Integer> palveluiden_lkmColumn;


//*************************************************
@FXML
private void etsiInfo (ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
    try {
        paaIkkuna pi = paaIkkunaDAO.etsiInfo(txtVarausID.getText());
        populateAndShowPaaikkuna(pi);
    } catch (SQLException e){
        System.out.println("Error" + e);
        throw e;
    }
}

    @FXML
    private void etsiInfot(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        try {
            ObservableList<paaIkkuna> piData = paaIkkunaDAO.etsiInfot();
            populatePaaikkuna(piData);
        } catch (SQLException e) {
            System.out.println("Virhe: \n" + e);
            throw e;
        }

    }

    @FXML
    private void populatePaaikkuna(paaIkkuna pi) throws ClassNotFoundException {
        ObservableList<paaIkkuna> piData = FXCollections.observableArrayList();
        piData.add(pi);
        paaIkkunaTaulu.setItems(piData);
    }


    @FXML
    private void populateAndShowPaaikkuna(paaIkkuna pi) throws ClassNotFoundException {
        if (pi != null) {
            populatePaaikkuna(pi);
        } else {
            System.out.println("vituiks meni");
        }
    }

        @FXML
        private void populatePaaikkuna (ObservableList < paaIkkuna > piData) throws ClassNotFoundException {
            paaIkkunaTaulu.setItems(piData);
        }

//*********************************************


        //tietojen lisäys-/muokkausikkunan avaus
        @FXML
        private void mokki () throws Exception {

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("mokki.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.show();

        }

        @FXML
        private void openReport() throws Exception {

    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("report.fxml"));
    Parent root = (Parent) fxmlLoader.load();
    Stage stage = new Stage();
    stage.setScene(new Scene(root));
    stage.show();
        }


    @FXML
    private void exit(){
        Stage stage = (Stage) btnMainExit.getScene().getWindow();
        stage.close();
    }



}
//TODO hide ikkunoille