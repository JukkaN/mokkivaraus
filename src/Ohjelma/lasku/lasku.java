package Ohjelma.lasku;

import javafx.beans.property.*;

public class lasku {

    public lasku() {
        this.lasku_id = new SimpleIntegerProperty();
        this.varaus_id = new SimpleIntegerProperty();
        this.asiakas_id = new SimpleIntegerProperty();
        this.nimi = new SimpleStringProperty();
        this.lahiosoite = new SimpleStringProperty();
        this.postitoimipaikka = new SimpleStringProperty();
        this.postinro = new SimpleStringProperty();
        this.summa = new SimpleIntegerProperty();
    }

    private IntegerProperty lasku_id;
private IntegerProperty varaus_id;
private IntegerProperty asiakas_id;
private StringProperty nimi;
private StringProperty lahiosoite;
private StringProperty postitoimipaikka;
private StringProperty postinro;
private IntegerProperty summa;


    public int getLasku_id() {
        return lasku_id.get();
    }

    public IntegerProperty lasku_idProperty() {
        return lasku_id;
    }

    public void setLasku_id(int lasku_id) {
        this.lasku_id.set(lasku_id);
    }

    public int getVaraus_id() {
        return varaus_id.get();
    }

    public IntegerProperty varaus_idProperty() {
        return varaus_id;
    }

    public void setVaraus_id(int varaus_id) {
        this.varaus_id.set(varaus_id);
    }

    public int getAsiakas_id() {
        return asiakas_id.get();
    }

    public IntegerProperty asiakas_idProperty() {
        return asiakas_id;
    }

    public void setAsiakas_id(int asiakas_id) {
        this.asiakas_id.set(asiakas_id);
    }

    public String getNimi() {
        return nimi.get();
    }

    public StringProperty nimiProperty() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi.set(nimi);
    }

    public String getLahiosoite() {
        return lahiosoite.get();
    }

    public StringProperty lahiosoiteProperty() {
        return lahiosoite;
    }

    public void setLahiosoite(String lahiosoite) {
        this.lahiosoite.set(lahiosoite);
    }

    public String getPostitoimipaikka() {
        return postitoimipaikka.get();
    }

    public StringProperty postitoimipaikkaProperty() {
        return postitoimipaikka;
    }

    public void setPostitoimipaikka(String postitoimipaikka) {
        this.postitoimipaikka.set(postitoimipaikka);
    }

    public String getPostinro() {
        return postinro.get();
    }

    public StringProperty postinroProperty() {
        return postinro;
    }

    public void setPostinro(String postinro) {
        this.postinro.set(postinro);
    }

    public int getSumma() {
        return summa.get();
    }

    public IntegerProperty summaProperty() {
        return summa;
    }

    public void setSumma(int summa) {
        this.summa.set(summa);
    }
}
