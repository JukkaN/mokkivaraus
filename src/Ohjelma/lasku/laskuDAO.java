package Ohjelma.lasku;

import DB.dbConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class laskuDAO {

    public static ObservableList<lasku> etsiLaskut () throws SQLException, ClassNotFoundException {
        String selectStmt = "SELECT * FROM lasku";

        try {
            ResultSet rsLas = dbConnection.dbExecuteQuery(selectStmt);
            ObservableList<lasku> laskuData = getLaskuLista(rsLas);

            return laskuData;
        } catch (SQLException e) {
            System.out.println("Virhe: " + e);
            throw e;
        }
    }
    public static lasku etsiLasku (String lasku_id) throws SQLException, ClassNotFoundException {

        String selectStmt = "SELECT * FROM lasku WHERE lasku_id="+ lasku_id;
        try {
            ResultSet rsLa = dbConnection.dbExecuteQuery(selectStmt);
            lasku lasku = laskuRS(rsLa);
            return lasku;
        } catch (SQLException e) {
            System.out.println("Virhe: " + e);
            throw e;
        }

    }


    private static lasku laskuRS(ResultSet rs) throws SQLException
    {
        lasku la = null;
        if (rs.next()) {
            la = new lasku();
            la.setLasku_id(rs.getInt("lasku_id"));
            la.setVaraus_id(rs.getInt("varaus_id"));
            la.setAsiakas_id(rs.getInt("asiakas_id"));
            la.setNimi(rs.getString("nimi"));
            la.setLahiosoite(rs.getString("lahiosoite"));
            la.setPostitoimipaikka(rs.getString("postitoimipaikka"));
            la.setPostinro(rs.getString("postinro"));
            la.setSumma(rs.getInt("summa"));

        }
        return la;
    }



    private static ObservableList<lasku> getLaskuLista(ResultSet rs) throws SQLException, ClassNotFoundException {
        ObservableList<lasku> laskuList = FXCollections.observableArrayList();

        while (rs.next()) {
            lasku la = new lasku();
            la.setLasku_id(rs.getInt("lasku_id"));
            la.setVaraus_id(rs.getInt("varaus_id"));
            la.setAsiakas_id(rs.getInt("asiakas_id"));
            la.setNimi(rs.getString("nimi"));
            la.setLahiosoite(rs.getString("lahiosoite"));
            la.setPostitoimipaikka(rs.getString("postitoimipaikka"));
            la.setPostinro(rs.getString("postinro"));
            la.setSumma(rs.getInt("summa"));
            laskuList.add(la);
        }
        return laskuList;
    }

    public static void deleteLasku (String lasku_id) throws SQLException, ClassNotFoundException {

        String updateStmt =

                "  DELETE FROM lasku WHERE lasku_id ="+ lasku_id;

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }

    public static void insertLasku (String lasku_id, String varaus_id, String asiakas_id, String nimi, String lahiosoite, String postitoimipaikka, String postinro, String summa) throws SQLException, ClassNotFoundException {

        String updateStmt =

                "INSERT INTO lasku\n" +
                        "(lasku_id, varaus_id, asiakas_id, nimi, lahiosoite, postitoimipaikka, postinro, summa )\n" +
                        "VALUES\n" + "('"+lasku_id+"', '"+varaus_id+"', '"+asiakas_id+"','"+nimi+"','"+lahiosoite+"', '"+postitoimipaikka+"', '"+postinro+"', '"+summa+"') ";

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }

    public static void updateLasku (String lasku_id, String varaus_id, String asiakas_id, String nimi, String lahiosoite, String postitoimipaikka, String postinro, String summa) throws SQLException, ClassNotFoundException
    {

        String updateStmt =

                "UPDATE lasku\n" +
                        "SET lasku_id= '"+lasku_id+"', varaus_id='"+varaus_id+"', asiakas_id='"+asiakas_id+"', nimi='"+nimi+"', lahiosoite='"+lahiosoite+"', postitoimipaikka='"+postitoimipaikka+"', postinro='"+postinro+"', summa='"+summa+"' WHERE lasku_id='" + lasku_id + "'";

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }




}
