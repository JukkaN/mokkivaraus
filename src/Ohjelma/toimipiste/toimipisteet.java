package Ohjelma.toimipiste;

import javafx.beans.property.*;

public class toimipisteet {

    //constructor
    public toimipisteet() {
        this.toimipiste_id = new SimpleIntegerProperty();
        this.nimi = new SimpleStringProperty();
        this.lahiosoite = new SimpleStringProperty();
        this.postitoimipaikka = new SimpleStringProperty();
        this.postinro = new SimpleStringProperty();
        this.email = new SimpleStringProperty();
        this.puhelinnro = new SimpleStringProperty();
    }

    private IntegerProperty toimipiste_id;
    private StringProperty nimi;
    private StringProperty lahiosoite;
    private StringProperty postitoimipaikka;
    private StringProperty postinro;
    private StringProperty email;
    private StringProperty puhelinnro;

    public int getToimipiste_id() {
        return toimipiste_id.get();
    }

    public IntegerProperty toimipiste_idProperty() {
        return toimipiste_id;
    }

    public void setToimipiste_id(int toimipiste_id) {
        this.toimipiste_id.set(toimipiste_id);
    }

    public String getNimi() {
        return nimi.get();
    }

    public StringProperty nimiProperty() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi.set(nimi);
    }

    public String getLahiosoite() {
        return lahiosoite.get();
    }

    public StringProperty lahiosoiteProperty() {
        return lahiosoite;
    }

    public void setLahiosoite(String lahiosoite) {
        this.lahiosoite.set(lahiosoite);
    }

    public String getPostitoimipaikka() {
        return postitoimipaikka.get();
    }

    public StringProperty postitoimipaikkaProperty() {
        return postitoimipaikka;
    }

    public void setPostitoimipaikka(String postitoimipaikka) {
        this.postitoimipaikka.set(postitoimipaikka);
    }

    public String getPostinro() {
        return postinro.get();
    }

    public StringProperty postinroProperty() {
        return postinro;
    }

    public void setPostinro(String postinro) {
        this.postinro.set(postinro);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getPuhelinnro() {
        return puhelinnro.get();
    }

    public StringProperty puhelinnroProperty() {
        return puhelinnro;
    }

    public void setPuhelinnro(String puhelinnro) {
        this.puhelinnro.set(puhelinnro);
    }
}
