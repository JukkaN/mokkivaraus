package Ohjelma.toimipiste;

import DB.dbConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class toimipisteetDAO {

    public static ObservableList<toimipisteet> etsiToimipisteet () throws SQLException, ClassNotFoundException {
        String selectStmt = "SELECT * FROM toimipiste";

        try {
            ResultSet rsTpt = dbConnection.dbExecuteQuery(selectStmt);
            ObservableList<toimipisteet> tpData = getToimipisteLista(rsTpt);

            return tpData;
        } catch (SQLException e) {
            System.out.println("Virhe: " + e);
            throw e;
        }
    }
        public static toimipisteet etsiToimipiste (String toimipiste_id) throws SQLException, ClassNotFoundException {

            String selectStmt = "SELECT * FROM toimipiste WHERE toimipiste_id=" + toimipiste_id;
            try {
                ResultSet rsTp = dbConnection.dbExecuteQuery(selectStmt);
                toimipisteet toimipiste = toimipisteRS(rsTp);
                return toimipiste;
            } catch (SQLException e) {
                System.out.println("Virhe: " + e);
                throw e;
            }

        }


        private static toimipisteet toimipisteRS (ResultSet rs) throws SQLException
        {
            toimipisteet tp = null;
            if (rs.next()) {
                tp = new toimipisteet();
                tp.setToimipiste_id(rs.getInt("toimipiste_id"));
                tp.setNimi(rs.getString("nimi"));
                tp.setLahiosoite(rs.getString("lahiosoite"));
                tp.setPostitoimipaikka(rs.getString("postitoimipaikka"));
                tp.setPostinro(rs.getString("postinro"));
                tp.setEmail(rs.getString("email"));
                tp.setPuhelinnro(rs.getString("puhelinnro"));

            }
            return tp;
        }


        private static ObservableList<toimipisteet> getToimipisteLista (ResultSet rs) throws
        SQLException, ClassNotFoundException {
            ObservableList<toimipisteet> tpList = FXCollections.observableArrayList();

            while (rs.next()) {
                toimipisteet tp = new toimipisteet();
                tp.setToimipiste_id(rs.getInt("toimipiste_id"));
                tp.setNimi(rs.getString("nimi"));
                tp.setLahiosoite(rs.getString("lahiosoite"));
                tp.setPostitoimipaikka(rs.getString("postitoimipaikka"));
                tp.setPostinro(rs.getString("postinro"));
                tp.setEmail(rs.getString("email"));
                tp.setPuhelinnro(rs.getString("puhelinnro"));
                tpList.add(tp);
            }
            return tpList;
        }

        public static void deleteToimipiste (String toimipiste_id) throws SQLException, ClassNotFoundException {

            String updateStmt =

                    "  DELETE FROM toimipiste WHERE toimipiste_id =" + toimipiste_id;

            try {
                dbConnection.dbExecuteUpdate(updateStmt);
            } catch (SQLException e) {
                System.out.print("Virhe: " + e);
                throw e;
            }
        }

        public static void insertToimipiste (String toimipiste_id, String nimi, String lahiosoite, String
        postitoimipaikka, String postinro, String email, String puhelinnro) throws SQLException, ClassNotFoundException
        {

            String updateStmt =

                    "INSERT INTO toimipiste\n" +
                            "(toimipiste_id, nimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro )\n" +
                            "VALUES\n" + "('" + toimipiste_id + "', '" + nimi + "', '" + lahiosoite + "','" + postitoimipaikka + "', '" + postinro + "', '" + email + "', '" + puhelinnro + "') ";

            try {
                dbConnection.dbExecuteUpdate(updateStmt);
            } catch (SQLException e) {
                System.out.print("Virhe: " + e);
                throw e;
            }
        }

        public static void updateToimipiste (String toimipiste_id, String nimi, String lahiosoite, String
                postitoimipaikka, String postinro, String email, String puhelinnro) throws SQLException, ClassNotFoundException
        {

            String updateStmt =

                    "UPDATE toimipiste\n" +
                            "SET toimipiste_id= '"+toimipiste_id+"', nimi='"+nimi+"', lahiosoite='"+lahiosoite+"', postitoimipaikka='"+postitoimipaikka+"', postinro='"+postinro+"', email='"+email+"', puhelinnro='"+puhelinnro+"' WHERE toimipiste_id='" + toimipiste_id + "'";

            try {
                dbConnection.dbExecuteUpdate(updateStmt);
            } catch (SQLException e) {
                System.out.print("Virhe: " + e);
                throw e;
            }
        }




    }
//TODO FUNC poista valittu toimipiste
