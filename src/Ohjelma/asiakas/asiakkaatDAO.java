package Ohjelma.asiakas;

import DB.dbConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class asiakkaatDAO {

    public static ObservableList<asiakkaat> etsiAsiakkaat () throws SQLException, ClassNotFoundException {
        String selectStmt = "SELECT * FROM asiakas";

        try {
            ResultSet rsAsi = dbConnection.dbExecuteQuery(selectStmt);
            ObservableList<asiakkaat> asData = getAsiakasLista(rsAsi);

            return asData;
        } catch (SQLException e) {
            System.out.println("Virhe: " + e);
            throw e;
        }
    }
        public static asiakkaat etsiAsiakas (String asiakas_id) throws SQLException, ClassNotFoundException {

            String selectStmt = "SELECT * FROM asiakas WHERE asiakas_id="+ asiakas_id;
            try {
                ResultSet rsAs = dbConnection.dbExecuteQuery(selectStmt);
                asiakkaat asiakas = asiakasRS(rsAs);
                return asiakas;
            } catch (SQLException e) {
                System.out.println("Virhe: " + e);
                throw e;
            }

        }


        private static asiakkaat asiakasRS(ResultSet rs) throws SQLException
        {
            asiakkaat as = null;
            if (rs.next()) {
                as = new asiakkaat();
                as.setAsiakas_id(rs.getInt("asiakas_id"));
                as.setEtunimi(rs.getString("etunimi"));
                as.setSukunimi(rs.getString("sukunimi"));
                as.setLahiosoite(rs.getString("lahiosoite"));
                as.setPostitoimipaikka(rs.getString("postitoimipaikka"));
                as.setPostinro(rs.getString("postinro"));
                as.setEmail(rs.getString("email"));
                as.setPuhelinnro(rs.getString("puhelinnro"));

            }
            return as;
        }



        private static ObservableList<asiakkaat> getAsiakasLista(ResultSet rs) throws SQLException, ClassNotFoundException {
            ObservableList<asiakkaat> asList = FXCollections.observableArrayList();

            while (rs.next()) {
                asiakkaat as = new asiakkaat();
                as.setAsiakas_id(rs.getInt("asiakas_id"));
                as.setEtunimi(rs.getString("etunimi"));
                as.setSukunimi(rs.getString("sukunimi"));
                as.setLahiosoite(rs.getString("lahiosoite"));
                as.setPostitoimipaikka(rs.getString("postitoimipaikka"));
                as.setPostinro(rs.getString("postinro"));
                as.setEmail(rs.getString("email"));
                as.setPuhelinnro(rs.getString("puhelinnro"));
                asList.add(as);
            }
            return asList;
        }

        public static void deleteAsiakas (String asiakas_id) throws SQLException, ClassNotFoundException {

            String updateStmt =

                    "  DELETE FROM asiakas WHERE asiakas_id ="+ asiakas_id;

            try {
                dbConnection.dbExecuteUpdate(updateStmt);
            } catch (SQLException e) {
                System.out.print("Virhe: " + e);
                throw e;
            }
        }

        public static void insertAsiakas (String asiakas_id, String etunimi, String sukunimi, String lahiosoite, String postitoimipaikka, String postinro, String email, String puhelinnro) throws SQLException, ClassNotFoundException {

            String updateStmt =

                    "INSERT INTO asiakas\n" +
                            "(asiakas_id, etunimi, sukunimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro )\n" +
                            "VALUES\n" + "('"+asiakas_id+"', '"+etunimi+"', '"+sukunimi+"','"+lahiosoite+"','"+postitoimipaikka+"', '"+postinro+"', '"+email+"', '"+puhelinnro+"') ";

            try {
                dbConnection.dbExecuteUpdate(updateStmt);
            } catch (SQLException e) {
                System.out.print("Virhe: " + e);
                throw e;
            }
        }

    public static void updateAsiakas (String asiakas_id, String etunimi, String sukunimi, String lahiosoite, String postitoimipaikka, String postinro, String email, String puhelinnro) throws SQLException, ClassNotFoundException
    {

        String updateStmt =

                "UPDATE asiakas\n" +
                        "SET asiakas_id= '"+asiakas_id+"', etunimi='"+etunimi+"', sukunimi='"+sukunimi+"', lahiosoite='"+lahiosoite+"', postitoimipaikka='"+postitoimipaikka+"', postinro='"+postinro+"', email='"+email+"', puhelinnro='"+puhelinnro+"' WHERE asiakas_id='" + asiakas_id + "'";

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }

//TODO FUNC poista valittu asiakas


    }


