package Ohjelma.palvelu;

import DB.dbConnection;
import Ohjelma.palvelu.palvelu;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class palveluDAO {

    public static ObservableList<palvelu> etsiPalvelut () throws SQLException, ClassNotFoundException {
        String selectStmt = "SELECT * FROM palvelu";

        try {
            ResultSet rsPal = dbConnection.dbExecuteQuery(selectStmt);
            ObservableList<palvelu> paData = getPalveluLista(rsPal);

            return paData;
        } catch (SQLException e) {
            System.out.println("Virhe: " + e);
            throw e;
        }
    }

    public static palvelu etsiPalvelu (String palvelu_id) throws SQLException, ClassNotFoundException {

        String selectStmt = "SELECT * FROM palvelu WHERE palvelu_id="+ palvelu_id;
        try {
            ResultSet rsPa = dbConnection.dbExecuteQuery(selectStmt);
            palvelu palvelu = palveluRS(rsPa);
            return palvelu;
        } catch (SQLException e) {
            System.out.println("Virhe: " + e);
            throw e;
        }

    }


    private static palvelu palveluRS(ResultSet rs) throws SQLException
    {
        palvelu pa = null;
        if (rs.next()) {
            pa = new palvelu();
            pa.setPalvelu_id(rs.getInt("palvelu_id"));
            pa.setPa_tpid(rs.getInt("toimipiste_id"));
            pa.setNimi(rs.getString("nimi"));
            pa.setTyyppi(rs.getString("tyyppi"));
            pa.setKuvaus(rs.getString("kuvaus"));
            pa.setHinta(rs.getString("hinta"));


        }
        return pa;
    }



    private static ObservableList<palvelu> getPalveluLista(ResultSet rs) throws SQLException, ClassNotFoundException {
        ObservableList<palvelu> paList = FXCollections.observableArrayList();

        while (rs.next()) {
            palvelu pa = new palvelu();
            pa.setPalvelu_id(rs.getInt("palvelu_id"));
            pa.setPa_tpid(rs.getInt("toimipiste_id"));
            pa.setNimi(rs.getString("nimi"));
            pa.setTyyppi(rs.getString("tyyppi"));
            pa.setKuvaus(rs.getString("kuvaus"));
            pa.setHinta(rs.getString("hinta"));
            paList.add(pa);
        }
        return paList;
    }

    public static void deletePalvelu (String palvelu_id) throws SQLException, ClassNotFoundException {

        String updateStmt =

                "  DELETE FROM palvelu WHERE palvelu_id ="+ palvelu_id;

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }

    public static void insertPalvelu (String palvelu_id, String toimipiste_id, String nimi, String tyyppi, String kuvaus, String hinta) throws SQLException, ClassNotFoundException {

        String updateStmt =

                "INSERT INTO palvelu\n" +
                        "(palvelu_id, toimipiste_id, nimi, tyyppi, kuvaus, hinta )\n" +
                        "VALUES\n" + "('"+palvelu_id+"', '"+toimipiste_id+"', '"+nimi+"', '"+tyyppi+"','"+kuvaus+"','"+hinta+"') ";

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }

    public static void updatePalvelu (String palvelu_id, String toimipiste_id, String nimi, String tyyppi, String kuvaus, String hinta) throws SQLException, ClassNotFoundException
    {

        String updateStmt =

                "UPDATE palvelu\n" +
                        "SET palvelu_id= '"+palvelu_id+"', toimipiste_id='"+toimipiste_id+"', nimi='"+nimi+"', tyyppi='"+tyyppi+"', kuvaus='"+kuvaus+"', hinta='"+hinta+"' WHERE palvelu_id='" + palvelu_id + "'";

        try {
            dbConnection.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Virhe: " + e);
            throw e;
        }
    }


//TODO FUNC poista valittu palvelu


}