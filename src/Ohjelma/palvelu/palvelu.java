package Ohjelma.palvelu;

import javafx.beans.property.*;

public class palvelu {

    public palvelu() {
        this.palvelu_id = new SimpleIntegerProperty();
        this.toimipiste_id = new SimpleIntegerProperty();
        this.nimi = new SimpleStringProperty();
        this.tyyppi = new SimpleStringProperty();
        this.kuvaus = new SimpleStringProperty();
        this.hinta = new SimpleStringProperty();
    }

    private IntegerProperty palvelu_id;
    private IntegerProperty toimipiste_id;
    private StringProperty nimi;
    private StringProperty tyyppi;
    private StringProperty kuvaus;
    private StringProperty hinta;

    public int getPalvelu_id() {
        return palvelu_id.get();
    }

    public IntegerProperty palvelu_idProperty() {
        return palvelu_id;
    }

    public void setPalvelu_id(int palvelu_id) {
        this.palvelu_id.set(palvelu_id);
    }

    public String getNimi() {
        return nimi.get();
    }

    public StringProperty nimiProperty() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi.set(nimi);
    }

    public String getTyyppi() {
        return tyyppi.get();
    }

    public StringProperty tyyppiProperty() {
        return tyyppi;
    }

    public void setTyyppi(String tyyppi) {
        this.tyyppi.set(tyyppi);
    }

    public String getKuvaus() {
        return kuvaus.get();
    }

    public StringProperty kuvausProperty() {
        return kuvaus;
    }

    public void setKuvaus(String kuvaus) {
        this.kuvaus.set(kuvaus);
    }

    public String getHinta() {
        return hinta.get();
    }

    public StringProperty hintaProperty() {
        return hinta;
    }

    public void setHinta(String hinta) {
        this.hinta.set(hinta);
    }

    public int getToimipiste_id() {
        return toimipiste_id.get();
    }

    public IntegerProperty toimipiste_idProperty() {
        return toimipiste_id;
    }

    public void setPa_tpid(int toimipiste_id) {
        this.toimipiste_id.set(toimipiste_id);
    }
}
