module MokkiVaraus {

    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql.rowset;
    requires org.mariadb.jdbc;

    opens Ohjelma;
}