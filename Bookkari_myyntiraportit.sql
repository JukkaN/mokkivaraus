USE `village_bookkari_r16`;
-- by LL
-- Huom. palvelu.tyyppi int (kooditus alk. 10 tai alk. 20) kertoo onko mökki vai lisäpalvelu

-- MUISTILAPPU `varaus` -taulun sisällöstä
-- (`varaus_id`, `asiakas_id`, `toimipiste_id`, `varattu_pvm`, `vahvistus_pvm`, `varattu_alkupvm`, `varattu_loppupvm`)
	(1, 1, 1, '1.12.2007', '1.6.2018', '1.4.2017-6.4.2019'),
	(2, 3, 1, '6.5.2019', '9.5.2019', '10.5.2019-12.5.2019'),
	(3, 2, 2, '9.5.2019', '10.5.2019', '12.5.2019-14.5.2019');
-- MUISTILAPPU `varauksen_palvelut` -taulun sisällöstä
-- (`varaus_id`, `palvelu_id`, `lkm`)
	(1, 1, 2),
	(2, 2, 2),
	(3, 3, 2);	
-- Varauksen palveluissa palvelu_id:t 1,2,3 = kaikki mökkejä tyyppiä 11, eri toimipisteissä 1,2,3
	
-- MAJOITTUMISTEN (palvelu.tyyppi 11 - 19) raportointi aikajaksolla valituissa toimipisteissä

Ideana: Ota varatun palvelun toimipisteen nimi(toimipiste), id+nimi+kuvaus+hinta(palvelu) ja lkm(varauksen_palvelut) ja ajanjakso(varaus)
tietystä toimipisteestä(varaus=toimipiste) ja tietyllä aikavälillä(varaus) -- varchar? miten aikavälin haku...??

SELECT t.nimi AS 'toimipaikka', p.palvelu_id AS 'palvelun ID', p.nimi, p.kuvaus, p.hinta, vp.lkm, (p.hinta*vp.lkm) AS 'hinta yht', v.varattu_alkupvm, v.varattu_loppupvm
FROM varauksen_palvelut vp, palvelu p, varaus v, toimipiste t
WHERE (p.tyyppi < 20)
AND (v.toimipiste_id = 1) -- tai 2, tai 3 -> KÄYTTÄJÄN VALINTA TÄHÄN
AND (p.palvelu_id = vp.palvelu_id) -- liitosehto1; palvelu on sama kuin varauksen_palvelu
AND (v.varaus_id = vp.varaus_id) -- liitosehto2; varaukseen liittyy varauksen_palveluita
AND (v.toimipiste_id = t.toimipiste_id) -- liitosehto3; toimipiste on varauksella
ORDER BY t.nimi;


-- LISÄPALVELUJEN (palvelu.tyyppi 21 - 29) raportointi aikajaksolla valituissa toimipisteissä

SELECT t.nimi AS 'toimipaikka', p.palvelu_id AS 'palvelun ID', p.nimi, p.kuvaus, p.hinta, vp.lkm, (p.hinta*vp.lkm) AS 'hinta yht', v.varattu_alkupvm, v.varattu_loppupvm
FROM varauksen_palvelut vp, palvelu p, varaus v, toimipiste t
WHERE (p.tyyppi >= 20)
AND (v.toimipiste_id = 1) -- tai 2, tai 3 -> KÄYTTÄJÄN VALINTA TÄHÄN
AND (p.palvelu_id = vp.palvelu_id) -- liitosehto1; palvelu on sama kuin varauksen_palvelu
AND (v.varaus_id = vp.varaus_id) -- liitosehto2; varaukseen liittyy varauksen_palveluita
AND (v.toimipiste_id = t.toimipiste_id) -- liitosehto3; toimipiste on varauksella
ORDER BY t.nimi;

-- Huom. Lisäpalvelujen tyyppiä ei ole mallivarauksilla.
